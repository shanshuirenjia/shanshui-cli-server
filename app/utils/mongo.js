const Mongodb = require('@pick-star/cli-mongodb');

const { MONGODB_URL } = require('../../config/db');

function mongo() {
  return new Mongodb(MONGODB_URL);
}

module.exports = mongo;
